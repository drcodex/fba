<?php
/**
 * Name:    Terminals Model
 * Author:  DrCodeX Technologies
 */
defined('BASEPATH') OR exit('No direct script access allowed');
class Orders_model extends CI_Model
{
	public function __construct()
    {
		parent::__construct();
	}
	
	function get_objects($type)
	{
		$this->db->where('object_type', $type);
		$result = $this->db->get('objects');
		return $result->result_array();
	}

	function add_object($data)
	{
		$this->db->insert('objects', $data);
		return $this->db->insert_id();
	}
	function get_object($object_id)
	{
		$this->db->where('object_id', $object_id);
		$result = $this->db->get('objects');
		return $result->row_array();
	}
	function update_object($object_id, $data)
	{
		$this->db->where('object_id', $object_id);
		$this->db->update('objects', $data);
	}
	
	function delete_object($object_id)
	{
		$this->db->where('object_id', $object_id);
		$result = $this->db->delete('objects');
		return $result;
	}
	function add_objectmeta($objectmeta)
	{
		$this->db->insert_batch('objectmeta', $objectmeta);
	}
	
	function delete_order($object_id)
	{
		$this->db->where('object_id', $object_id);
		$result = $this->db->delete('orders');
		return $result;
	}
	function get_objectmeta($object_id)
	{
		$this->db->where('object_id', $object_id);
		$result = $this->db->get('objectmeta');
		return $result->result_array();
	}
	
	function get_objectmeta_by_key($meta_key, $object_id)
	{
		$this->db->where('meta_key', $meta_key);
		$this->db->where('object_id', $object_id);
		$result = $this->db->get('objectmeta');
		return $result->row_array();
	}
	function update_objectmeta($object_id, $objectmeta)
	{
		$this->db->where('object_id', $object_id);
		$this->db->update_batch('objectmeta', $objectmeta, 'ometa_id');
	}
	
	function delete_ordermeta($order_meta_id)
	{
		$this->db->where('order_meta_id', $order_meta_id);
		$this->db->delete('ordermeta');
	}
	
	function get_objectmeta_single($object_id)
	{
		$this->db->where('object_id', $object_id);
		$result = $this->db->get('objectmeta');
		foreach($result->result() as $row) {
			$options[$row->meta_key] = $row->meta_value;
		}
		if(!empty($options)){
			return $options;
		}
	}
	function get_ordermeta_single($order_id)
	{
		$this->db->where('order_id', $order_id);
		$result = $this->db->get('ordermeta');
		foreach($result->result() as $row) {
			$options[$row->meta_key] = $row->meta_value;
		}
		if(!empty($options)){
			return $options;
		}
	}
	function get_orders($type, $author)
	{
		$this->db->join('orders', 'orders.object_id = objects.object_id', 'inner');
		if ($type != NULL)
        {
			$this->db->where('object_type', $type);
        }
		if ($author != NULL)
        {
			$this->db->where('object_author', $author);
        }
		$result = $this->db->get('objects');
		return $result->result_array();
	}
	function get_order($type, $object_id)
	{
		$this->db->join('orders', 'orders.object_id = objects.object_id');
		$this->db->where('objects.object_type', $type);
		$this->db->where('objects.object_id', $object_id);
		$result = $this->db->get('objects');
		return $result->row_array();
	}
	
	function add_order($additional_data)
	{
		$this->db->insert('orders', $additional_data);
		return $this->db->insert_id();
	}
	
	function get_ordermeta($order_id)
	{
		$this->db->where('order_id', $order_id);
		$result = $this->db->get('ordermeta');
		return $result->result_array();
	}
	
	function get_ordermeta_by_key($meta_key, $order_id)
	{
		$this->db->where('meta_key', $meta_key);
		$this->db->where('order_id', $order_id);
		$result = $this->db->get('ordermeta');
		return $result->row_array();
	}
	function add_ordermeta($ordermeta)
	{
		$this->db->insert_batch('ordermeta', $ordermeta);
	}
	function update_ordermeta($order_id, $ordermeta)
	{
		$this->db->where('order_id', $order_id);
		$this->db->update_batch('ordermeta', $ordermeta, 'order_meta_id');
	}
	function update_ordermeta_by_key($order_id, $key, $ordermeta)
	{
		$this->db->where('order_id', $order_id);
		$this->db->where('meta_key', $key);
		$this->db->update('ordermeta', $ordermeta);
	}
}
