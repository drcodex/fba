<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Auth
 * @property Ion_auth|Ion_auth_model $ion_auth        The ION Auth spark
 * @property CI_Form_validation      $form_validation The form validation library
 */
class Orders extends User_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->library(array('ion_auth', 'form_validation'));
		$this->load->helper(array('url', 'language'));

		$this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));
		$this->load->model('orders_model');
		$this->load->model('currencies_model');
		$this->load->model('locations_model');
		$this->load->model('users_model');
		$this->load->model('products_model');
		$this->load->model('marketplaces_model');
		$this->load->model('couriers_model');
		$this->lang->load('auth');
		$this->access_type = 'user';
	}

	public function index()
	{
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect($this->access_type . '/login', 'refresh');
		}
		elseif (!$this->ion_auth->in_group($group = 3)) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
			$this->_render_error('errors' . DIRECTORY_SEPARATOR . '401', $this->data);
		}
		else
		{
			$role_id = $this->session->userdata('user_id');
			$orders = $this->users_model->get_role_joint($role_id, $taxonomy = 'user_order', $table = 'orders', $column = 'order_id', $is_object = TRUE);
			$timezone  = 'UP5';
			$nm_gmt_time = local_to_gmt(strtotime('+ 1 month'), $timezone);
			$tm_gmt_time = local_to_gmt(strtotime('this month'), $timezone);
			$nextmonth = date('F', $nm_gmt_time);
			$thismonth = date('F', $tm_gmt_time);
			$lastday = date(strtotime("1 .$nextmonth. 2020"));
			$firstday = date(strtotime("1 .$thismonth. 2020"));
			foreach($orders as $order) {
				if($order['object_date_gmt'] > $firstday && $order['object_date_gmt'] < $lastday ){
					$orderArr[] = $order;
				}
			}
			if(!empty($orderArr)){
				$this->data['orders'] = $this->get_orders($orderArr);
				//echo '<pre>'; print_r($this->data['orders']); echo '</pre>'; die();
			}
			//echo '<pre>'; print_r($tm_gmt_time); echo '</pre>'; die();
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
			//echo '<pre>'; print_r($this->data); echo '</pre>'; die();
			$this->_render_page($this->access_type . DIRECTORY_SEPARATOR . 'orders' . DIRECTORY_SEPARATOR . 'index', $this->data);
		}
	}
	
	public function ajax_tracking_status()
	{
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect($this->access_type . '/login', 'refresh');
		}
		elseif (!$this->ion_auth->in_group($group = 3)) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
			$this->_render_error('errors' . DIRECTORY_SEPARATOR . '401', $this->data);
		}
		else
		{
			if($_POST)
			{
				$object_id = $this->input->post('object_id');
				$order_id = $this->input->post('order_id');
				$tracking_number = $this->input->post('tracking_number');
				if(!empty($tracking_number) && isset($tracking_number))
				{
					$url = 'https://api-eu.dhl.com/track/shipments?trackingNumber='.$tracking_number;
					$data = array(
						"DHL-API-Key: vsqLgrATs9zb77GCXKIOmVylxBOzQKsh",
					);
					$curl = curl_init();
					curl_setopt_array($curl, array(
						CURLOPT_URL => $url,
						CURLOPT_RETURNTRANSFER => true,
						CURLOPT_ENCODING => "",
						CURLOPT_MAXREDIRS => 10,
						CURLOPT_TIMEOUT => 30,
						CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
						CURLOPT_CUSTOMREQUEST => "GET",
						CURLOPT_HTTPHEADER => $data,
					));
					$response = curl_exec($curl);
					$err = curl_error($curl);
					curl_close($curl);
					$result = json_decode($response);
					foreach($result->shipments as $shipment) {
						//echo '<pre>'; print_r($shipment); echo '</pre>'; die();
						if(isset($shipment->status->status)){
							$status = $shipment->status->statusCode;
						} else {
							$description = $shipment->status->description;
							if($description == 'Shipment information received'){
								$status = "information received";
							} else {
								$description = $shipment->status->description;
							}
						}
					}
					$data = array(
						'object_status' => $status,
					);
					$this->orders_model->update_object($object_id, $data);
					switch ($status) {
						case "received":
							$class = 'info';
							break;
						case "transit":
							$class = 'warning';
							break;
						case "delivered":
							$class = 'success';
							break;
						case "canceled":
							$class = 'danger';
							break;
						default:
							$class = 'info';
					}
					$this->data['status'] = $status;
					$this->data['class'] = $class;
					//echo '<pre>'; print_r($status); echo '</pre>'; die();
					$theHTMLResponse = $this->load->view($this->access_type . '/ajax_blocks/tracking_status', $this->data, true);
					$response = array('response'=>'yes','message'=>'Tracking status','content'=>$theHTMLResponse);
					$this->output->set_content_type('application/json');
					$this->output->set_output(json_encode($response));
				}
				else
				{
					$response = array('response'=>'no','message'=>'No tracking code assigned yet by shipper.');
					$this->output->set_content_type('application/json');
					$this->output->set_output(json_encode($response));
				}
			}
		}
	}

	public function edit_order($object_id)
	{
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect($this->access_type . '/login', 'refresh');
		}
		elseif (!$this->ion_auth->in_group($group = 3)) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
			$this->_render_error('errors' . DIRECTORY_SEPARATOR . '401', $this->data);
		}
		else
		{
			$object = $this->orders_model->get_order('order', $object_id);
			if($_POST)
			{
				$product_id = $this->input->post('autocomplete');
				$item_quantity = $this->input->post('item_quantity');
				$ship_by_date = $this->input->post('ship_by_date');
				$deliver_by_date = $this->input->post('deliver_by_date');
				$your_earning = $this->input->post('your_earning');
				$order_number = $this->input->post('order_number');
				$currency_id = $this->input->post('currency_id');
				$customer_name = $this->input->post('customer_name');
				$customer_phone = $this->input->post('customer_phone');
				$shipping_address = $this->input->post('shipping_address');
				$country_id = $this->input->post('country_id');
			
				// validate form input
				$this->form_validation->set_rules('item_quantity','Product Quantity','trim|required');
				$this->form_validation->set_rules('ship_by_date','Ship by Date','trim|required');
				$this->form_validation->set_rules('deliver_by_date','Deliver by Date','trim|required');
				$this->form_validation->set_rules('your_earning','Earnings','trim|required');
				$this->form_validation->set_rules('order_number','Order Number','trim|required');
				$this->form_validation->set_rules('currency_id','Currency','trim|required');
				$this->form_validation->set_rules('customer_name','Customer Name','trim|required');
				$this->form_validation->set_rules('customer_phone','Customer Phone','trim|required');
				$this->form_validation->set_rules('shipping_address','Shipping Address','trim|required');
				$this->form_validation->set_rules('country_id','Country','trim|required');
				if ($this->form_validation->run() === TRUE)
				{
					$user_id = $this->session->userdata('user_id');
					$timezone  = 'UP5';
					$gmt_time = local_to_gmt(time(), $timezone);
					$local_time = gmt_to_local($gmt_time, $timezone);
					$data = array(
						'object_modified' => $local_time,
						'object_modified_gmt' => $gmt_time,
					);
					$this->orders_model->update_object($object_id, $data);
					$ordermeta = $this->orders_model->get_ordermeta($object['order_id']);
					$order_array = array();
					foreach ($ordermeta as $key => $order){
						if(!empty($this->input->post($order['meta_key'])))
						{
							$array['meta_key'] = "'".$order['meta_key']."'";
							$array['post_values'] = $this->input->post($order['meta_key']);
							$order_array[] = array(
								'order_meta_id'  => $order['order_meta_id'],
								'meta_key' => $order['meta_key'],
								'meta_value' => $array['post_values'],
							);
						}
					}
					$this->orders_model->update_ordermeta($object['order_id'], $order_array);
					$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));
					redirect($this->access_type . "/orders/edit_order/".$object_id, 'refresh');
				}
			}
			$meta = $this->orders_model->get_ordermeta_single($object['order_id']);
			$this->data['item_name'] = array(
				'name' => 'item_name',
				'id' => 'item_name',
				'type' => 'text',
				'value' => $this->form_validation->set_value('item_name', !empty($meta['item_name']) ? $meta['item_name'] : ""),
				'class' => 'form-control',
				'placeholder' => 'Enter product name here',
			);
			$this->data['item_quantity'] = array(
				'name' => 'item_quantity',
				'id' => 'item_quantity',
				'type' => 'text',
				'value' => $this->form_validation->set_value('item_quantity', !empty($meta['item_quantity']) ? $meta['item_quantity'] : ""),
				'class' => 'form-control',
				'placeholder' => 'Enter product quantity here',
			);
			$this->data['ship_by_date'] = array(
				'name' => 'ship_by_date',
				'id' => 'ship_by',
				'type' => 'text',
				'value' => $this->form_validation->set_value('ship_by_date', !empty($meta['ship_by_date']) ? $meta['ship_by_date'] : ""),
				'class' => 'form-control',
				'placeholder' => 'Month, DD YYYY',
			);
			$this->data['deliver_by_date'] = array(
				'name' => 'deliver_by_date',
				'id' => 'deliver_by',
				'type' => 'text',
				'value' => $this->form_validation->set_value('deliver_by_date', !empty($meta['deliver_by_date']) ? $meta['deliver_by_date'] : ""),
				'class' => 'form-control',
				'placeholder' => 'Month, DD YYYY',
			);
			$this->data['your_earning'] = array(
				'name' => 'your_earning',
				'id' => 'your_earning',
				'type' => 'text',
				'value' => $this->form_validation->set_value('your_earning', !empty($meta['your_earning']) ? $meta['your_earning'] : ""),
				'class' => 'form-control',
				'placeholder' => 'Enter your earning here',
			);
			$this->data['order_number'] = array(
				'name' => 'order_number',
				'id' => 'order_number',
				'type' => 'text',
				'value' => $this->form_validation->set_value('order_number', !empty($meta['order_number']) ? $meta['order_number'] : ""),
				'class' => 'form-control',
				'placeholder' => 'Enter marketplace order number here',
			);
			$this->data['customer_name'] = array(
				'name' => 'customer_name',
				'id' => 'customer_name',
				'type' => 'text',
				'value' => $this->form_validation->set_value('customer_name', !empty($meta['customer_name']) ? $meta['customer_name'] : ""),
				'class' => 'form-control',
				'placeholder' => 'Enter customer name here',
			);
			$this->data['customer_phone'] = array(
				'name' => 'customer_phone',
				'id' => 'customer_phone',
				'type' => 'text',
				'value' => $this->form_validation->set_value('customer_phone', !empty($meta['customer_phone']) ? $meta['customer_phone'] : ""),
				'class' => 'form-control',
				'placeholder' => 'Enter customer phone here',
			);
			$this->data['shipping_address'] = array(
				'name' => 'shipping_address',
				'id' => 'shipping_address',
				'type' => 'text',
				'value' => $this->form_validation->set_value('shipping_address', !empty($meta['shipping_address']) ? $meta['shipping_address'] : ""),
				'class' => 'form-control',
				'placeholder' => 'Enter shipping address here',
			);
			$this->data['product'] = $object;
			$this->data['ordermeta'] = $meta;
			//echo '<pre>'; print_r($this->data['ordermeta']); echo '</pre>'; die();
			$this->data['countries'] = $this->locations_model->get_all_countries();
			$this->data['currencies'] = $this->currencies_model->get_currencies();
			$this->data['vendors'] = $this->ion_auth->users(4)->result();
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
			$this->_render_page($this->access_type . DIRECTORY_SEPARATOR . 'orders' . DIRECTORY_SEPARATOR . 'edit_order', $this->data);
		}
	}
	public function place_order()
	{
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect($this->access_type . '/login', 'refresh');
		}
		elseif (!$this->ion_auth->in_group($group = 3)) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
			$this->_render_error('errors' . DIRECTORY_SEPARATOR . '401', $this->data);
		}
		else
		{
			$user_id = $this->session->userdata('user_id');
			if($_POST)
			{
				$product_id = $this->input->post('autocomplete');
				$product = $this->products_model->get_object($product_id);
				$item_name = $this->input->post('item_name');
				$item_quantity = $this->input->post('item_quantity');
				$ship_by_date = $this->input->post('ship_by_date');
				$deliver_by_date = $this->input->post('deliver_by_date');
				$your_earning = $this->input->post('your_earning');
				$order_number = $this->input->post('order_number');
				$currency_id = $this->input->post('currency_id');
				$customer_name = $this->input->post('customer_name');
				$customer_phone = $this->input->post('customer_phone');
				$shipping_address = $this->input->post('shipping_address');
				$country_id = $this->input->post('country_id');
				$vendor_id = $this->input->post('vendor_id');
				$shipper_id = $this->input->post('shipper_id');
			
				// validate form input
				$this->form_validation->set_rules('item_name','Product Name','trim|required');
				$this->form_validation->set_rules('item_quantity','Product Quantity','trim|required');
				$this->form_validation->set_rules('ship_by_date','Ship by Date','trim|required');
				$this->form_validation->set_rules('deliver_by_date','Deliver by Date','trim|required');
				$this->form_validation->set_rules('your_earning','Earnings','trim|required');
				$this->form_validation->set_rules('order_number','Order Number','trim|required');
				$this->form_validation->set_rules('currency_id','Currency','trim|required');
				$this->form_validation->set_rules('customer_name','Customer Name','trim|required');
				$this->form_validation->set_rules('customer_phone','Customer Phone','trim|required');
				$this->form_validation->set_rules('shipping_address','Shipping Address','trim|required');
				$this->form_validation->set_rules('country_id','Country','trim|required');
				$this->form_validation->set_rules('vendor_id','Vendor','trim|required');
				$this->form_validation->set_rules('shipper_id','Shipper','trim|required');
				if ($this->form_validation->run() === TRUE)
				{
					$timezone  = 'UP5';
					$gmt_time = local_to_gmt(time(), $timezone);
					$local_time = gmt_to_local($gmt_time, $timezone);
					$data = array(
						'object_type ' => 'order',
						'object_title' => $item_name,
						'object_content' => '',
						'object_status' => 'pending',
						'object_date' => $local_time,
						'object_date_gmt' => $gmt_time,
						'object_modified' => $local_time,
						'object_modified_gmt' => $gmt_time,
						'object_author' => $user_id,
					);
					$object_id = $this->orders_model->add_object($data);
					$additional_data = array(
						'order_name ' => $item_name,
						'order_type' => 'amazon_order',
						'object_id' => $object_id
					);
					$order_id = $this->orders_model->add_order($additional_data);
					$user_account = $this->get_user_account($user_id);
					$order_serial = $this->order_serial($user_id, $order_id, $user_account['order_prefix'], $timezone);
					//echo '<pre>'; print_r($order_serial); echo '</pre>'; die();
					$ordermeta = array(
						array(
							'order_id' => $order_id,
							'meta_key' => 'item_quantity',
							'meta_value' => $item_quantity,
						),
						array(
							'order_id' => $order_id,
							'meta_key' => 'ship_by_date',
							'meta_value' => $ship_by_date,
						),
						array(
							'order_id' => $order_id,
							'meta_key' => 'deliver_by_date',
							'meta_value' => $deliver_by_date,
						),
						array(
							'order_id' => $order_id,
							'meta_key' => 'your_earning',
							'meta_value' => $your_earning,
						),
						array(
							'order_id' => $order_id,
							'meta_key' => 'order_number',
							'meta_value' => $order_number,
						),
						array(
							'order_id' => $order_id,
							'meta_key' => 'customer_name',
							'meta_value' => $customer_name,
						),
						array(
							'order_id' => $order_id,
							'meta_key' => 'customer_phone',
							'meta_value' => $customer_phone,
						),
						array(
							'order_id' => $order_id,
							'meta_key' => 'shipping_address',
							'meta_value' => $shipping_address,
						),
						array(
							'order_id' => $order_id,
							'meta_key' => 'currency_id',
							'meta_value' => $currency_id,
						),
						array(
							'order_id' => $order_id,
							'meta_key' => 'country_id',
							'meta_value' => $country_id,
						),
						array(
							'order_id' => $order_id,
							'meta_key' => 'vendor_id',
							'meta_value' => $vendor_id,
						),
						array(
							'order_id' => $order_id,
							'meta_key' => 'shipper_id',
							'meta_value' => $shipper_id,
						),
						array(
							'order_id' => $order_id,
							'meta_key' => 'tracking_number',
							'meta_value' => '',
						),
						array(
							'order_id' => $order_id,
							'meta_key' => 'order_prefix',
							'meta_value' => $user_account['order_prefix'],
						),
						array(
							'order_id' => $order_id,
							'meta_key' => 'order_serial',
							'meta_value' => $order_serial,
						),
					);
					$product = $this->products_model->get_object($product_id);
					if($product['object_type'] == 'product_variation'){
						$productmeta = array(
							array(
								'order_id' => $order_id,
								'meta_key' => 'product_id',
								'meta_value' => $product['object_parent'],
							),
							array(
								'order_id' => $order_id,
								'meta_key' => 'variation_id',
								'meta_value' => $product['object_id'],
							),
						);
					}else{
						$productmeta = array(
							array(
								'order_id' => $order_id,
								'meta_key' => 'product_id',
								'meta_value' => $product['object_id'],
							),
						);
					}
					$order_meta = array_merge($ordermeta, $productmeta);
					//echo '<pre>'; print_r($order_meta); echo '</pre>'; die();
					$this->orders_model->add_ordermeta($order_meta);
					$args = array(
						'role_id' => $user_id,
						'taxonomy' => 'user_order',
						'parent' => NULL,
						'single' => TRUE,
					);
					$role_taxonomy = $this->users_model->get_role_taxonomy($args);
					//echo '<pre>'; print_r($role_taxonomy); echo '</pre>'; die();
					$relation_data = array(
						'joint_id' => $order_id,
						'role_taxonomy_id' => $role_taxonomy['role_taxonomy_id'],
					);
					$this->users_model->add_role_relationships($relation_data);
					$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));
					redirect($this->access_type . "/orders/view_orders", 'refresh');
					//$this->user_model->update_user_data($additional_data);
				}
			}
			$this->data['item_name'] = array(
				'name' => 'item_name',
				'id' => 'item_name',
				'type' => 'text',
				'value' => $this->form_validation->set_value('item_name'),
				'class' => 'form-control',
				'placeholder' => 'Enter product name here',
			);
			$this->data['item_quantity'] = array(
				'name' => 'item_quantity',
				'id' => 'item_quantity',
				'type' => 'text',
				'value' => $this->form_validation->set_value('item_quantity'),
				'class' => 'form-control',
				'placeholder' => 'Enter product quantity here',
			);
			$this->data['ship_by_date'] = array(
				'name' => 'ship_by_date',
				'id' => 'ship_by',
				'type' => 'text',
				'value' => $this->form_validation->set_value('ship_by_date'),
				'class' => 'form-control',
				'placeholder' => 'Month, DD YYYY',
			);
			$this->data['deliver_by_date'] = array(
				'name' => 'deliver_by_date',
				'id' => 'deliver_by',
				'type' => 'text',
				'value' => $this->form_validation->set_value('deliver_by_date'),
				'class' => 'form-control',
				'placeholder' => 'Month, DD YYYY',
			);
			$this->data['your_earning'] = array(
				'name' => 'your_earning',
				'id' => 'your_earning',
				'type' => 'text',
				'value' => $this->form_validation->set_value('your_earning'),
				'class' => 'form-control',
				'placeholder' => 'Enter your earning here',
			);
			$this->data['order_number'] = array(
				'name' => 'order_number',
				'id' => 'order_number',
				'type' => 'text',
				'value' => $this->form_validation->set_value('order_number'),
				'class' => 'form-control',
				'placeholder' => 'Enter marketplace order number here',
			);
			$this->data['customer_name'] = array(
				'name' => 'customer_name',
				'id' => 'customer_name',
				'type' => 'text',
				'value' => $this->form_validation->set_value('customer_name'),
				'class' => 'form-control',
				'placeholder' => 'Enter customer name here',
			);
			$this->data['customer_phone'] = array(
				'name' => 'customer_phone',
				'id' => 'customer_phone',
				'type' => 'text',
				'value' => $this->form_validation->set_value('customer_phone'),
				'class' => 'form-control',
				'placeholder' => 'Enter customer phone here',
			);
			$this->data['shipping_address'] = array(
				'name' => 'shipping_address',
				'id' => 'shipping_address',
				'type' => 'text',
				'value' => $this->form_validation->set_value('shipping_address'),
				'class' => 'form-control',
				'placeholder' => 'Enter shipping address here',
			);
			$this->data['countries'] = $this->locations_model->get_all_countries();
			$this->data['currencies'] = $this->currencies_model->get_currencies();
			$this->data['vendors'] = $this->ion_auth->users(4)->result();
			$this->data['shippers'] = $this->ion_auth->users(5)->result();
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
			//echo '<pre>'; print_r($this->data); echo '</pre>'; die();
			$this->_render_page($this->access_type . DIRECTORY_SEPARATOR . 'orders' . DIRECTORY_SEPARATOR . 'place_order', $this->data);
		}
	}
	public function delete_order($object_id)
	{

		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect($this->access_type . '/login', 'refresh');
		}
		else if (!$this->ion_auth->in_group($group = 3)) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
			$this->_render_error('errors' . DIRECTORY_SEPARATOR . '401', $this->data);
		}
		else
		{			
			if(!empty($object_id))
			{
				$this_order = $this->orders_model->get_order('order', $object_id);
				$ordermeta = $this->orders_model->get_ordermeta($this_order['order_id']);
				if(!empty($ordermeta))
				{
					foreach ($ordermeta as $key => $value)
					{
						$order_meta_id = $value['order_meta_id'];
						$this->orders_model->delete_ordermeta($order_meta_id);
						//echo '<pre>'; print_r($order_meta_id); echo '</pre>'; die();
					}
				}
				$this->orders_model->delete_object($object_id);
				$this->orders_model->delete_order($object_id);
				redirect($this->access_type . "/orders/view_orders", 'refresh');	
			}
		}
	}

	public function order_detail($object_id)
	{
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect($this->access_type . '/login', 'refresh');
		}
		elseif (!$this->ion_auth->in_group($group = 3)) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
			$this->_render_error('errors' . DIRECTORY_SEPARATOR . '401', $this->data);
		}
		else
		{
			$member_id = $this->session->userdata('user_id');
			if($_POST)
			{
				$this->form_validation->set_rules('date_created','Shipper','trim|required');
				$this->form_validation->set_rules('order_status','Tracking Number','trim|required');
				if ($this->form_validation->run() === TRUE)
				{
					$date_created = $this->input->post('date_created');
					$order_status = $this->input->post('order_status');
					$timezone  = 'UP5';
					$gmt_time = local_to_gmt($date_created, $timezone);
					$local_time = gmt_to_local($gmt_time, $timezone);
					$data = array(
						'object_status' => $order_status,
						'object_date' => $local_time,
						'object_date_gmt' => $gmt_time,
					);
					$this->orders_model->update_object($object_id, $data);
					$this->session->set_flashdata('class', 'btn btn-success waves-effect waves-light');
					$this->session->set_flashdata('message', 'Order detail is updated');
					redirect($this->access_type . "/orders/order_detail/".$object_id, 'refresh');
				}
			}
			$object = $this->orders_model->get_order('order', $object_id);
			$order = $this->get_order($object);
			
			$this->data['date_created'] = array(
				'name' => 'date_created',
				'id' => 'date_created',
				'type' => 'text',
				'value' => $this->form_validation->set_value('object_date', !empty($object['object_date']) ? $object['object_date'] : ""),
				'class' => 'form-control',
				'placeholder' => 'Month, DD YYYY',
			);
			$this->data['object_status'] = array(
				'name' => 'object_status',
				'id' => 'object_status',
				'type' => 'text',
				'value' => $this->form_validation->set_value('object_status', !empty($object['object_status']) ? $object['object_status'] : ""),
				'class' => 'form-control',
				'disabled' => 'disabled',
			);
			$this->data['order'] = $order;
			//echo '<pre>'; print_r($this->data['order']); echo '</pre>'; die();
			
			$this->data['class'] = $this->session->flashdata('class');
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
			$this->_render_page($this->access_type . DIRECTORY_SEPARATOR . 'orders' . DIRECTORY_SEPARATOR . 'order_detail', $this->data);
		}
	}
	
	private function get_order($value) {
		if(!empty($value)){
			$order_meta = $this->orders_model->get_ordermeta($value['order_id']);
			//echo '<pre>'; print_r($order_meta); echo '</pre>'; die();
			$datestring = 'M, d Y';		
			$orderArr['object_id'] = $value['object_id'];
			$orderArr['object_type'] = $value['object_type'];
			$orderArr['object_parent'] = $value['object_parent'];
			$orderArr['object_status'] = $value['object_status'];
			$orderArr['object_title'] = $value['object_title'];
			$orderArr['object_author'] = $value['object_author'];
			$orderArr['object_content'] = $value['object_content'];
			$orderArr['object_excerpt'] = $value['object_excerpt'];
			$orderArr['object_slug'] = $value['object_slug'];
			$orderArr['comment_status'] = $value['comment_status'];
			$orderArr['comment_count'] = $value['comment_count'];
			$orderArr['object_date'] = date($datestring, $value['object_date']);
			$orderArr['object_date_gmt'] = date($datestring, $value['object_date_gmt']);
			$orderArr['object_modified'] = date($datestring, $value['object_modified']);
			$orderArr['object_modified_gmt'] = date($datestring, $value['object_modified_gmt']);
			$orderArr['menu_order'] = $value['menu_order'];
			$orderArr['order_id'] = $value['order_id'];
			$orderArr['order_type'] = $value['order_type'];
			if(!empty($order_meta)){
				foreach ($order_meta as $key => $item) {
					$orderArr[$item['meta_key']] = $item['meta_value'];
					if($item['meta_key'] == 'country_id')
					{
						$country = $this->locations_model->get_country_by_id($item['meta_value']);
						$orderArr['country_id'] = $country->country_id;
						$orderArr['country_name'] = $country->country_name;
					}
					if($item['meta_key'] == 'vendor_id')
					{
						$vendor = $this->ion_auth->user($item['meta_value'])->row();
						if(!empty($vendor->company))
						{
							$orderArr['vendor_id'] = $vendor->id;
							$orderArr['vendor_name'] = $vendor->company;
						}
					}
					if($item['meta_key'] == 'shipper_id')
					{
						$shipper = $this->ion_auth->user($item['meta_value'])->row();
						if(!empty($shipper->company))
						{
							$orderArr['shipper_id'] = $shipper->id;
							$orderArr['shipper_name'] = $shipper->company;
						}
					}
					if($item['meta_key'] == 'courier_id')
					{
						$courier = $this->couriers_model->get_courier($item['meta_value']);
						if(!empty($courier))
						{
							$orderArr['courier_id'] = $courier->courier_id;
						}
					}
					if($item['meta_key'] == 'ship_by_date')
					{
						$orderArr['ship_by_date'] = date($datestring, $item['meta_value']);
					}
					if($item['meta_key'] == 'deliver_by_date')
					{
						$orderArr['deliver_by_date'] = date($datestring, $item['meta_value']);
					}
					$orderArray = $orderArr;
				}
			}
			if(!empty($orderArray)){
			    return $orderArray;
			}
		}
	}

	private function get_user_account($id) {
		$member = $this->users_model->get_user_by_id($id);
		$member_metadata = $this->users_model->get_user_metadata($id);
		if(!empty($member_metadata)){
			foreach ($member_metadata as $key => $item) {
				$flight_array = array();
				$itemArr[$item['meta_key']] = $item['meta_value'];
				if($item['meta_key'] == 'country_id')
				{
					$country = $this->locations_model->get_country_by_id($item['meta_value']);
					$itemArr['country_name'] = $country->country_name;
					$itemArr['continent_name'] = $country->continent_name;
				}
				if($item['meta_key'] == 'currency_id')
				{
					$currency = $this->currencies_model->get_currency_by_id($item['meta_value']);
					$itemArr['currency_name'] = $currency['currency_name'];
					$itemArr['currency_code'] = $currency['currency_code'];
					$itemArr['currency_symbol'] = $currency['currency_symbol'];
					//echo '<pre>'; print_r($currency); echo '</pre>'; die();
				}
				if($item['meta_key'] == 'marketplace_id')
				{
					$currency = $this->marketplaces_model->get_marketplace_by_id($item['meta_value']);
					$itemArr['marketplace_name'] = isset($currency['marketplace_name']);
					//echo '<pre>'; print_r($currency); echo '</pre>'; die();
				}
				$member_array = $itemArr;
			}
			
			$user_account = array_merge($member, $member_array);
			return $user_account;
		}
	}
	private function get_orders($orders){
		if(!empty($orders)){
			foreach ($orders as $key => $value) {
				$ordermeta = $this->orders_model->get_ordermeta($value['order_id']);
				foreach ($ordermeta as $key => $item) {
					$product_array = array();
					$itemArr[$item['meta_key']] = $item['meta_value'];
					if($item['meta_key'] == 'country_id')
					{
						$country = $this->locations_model->get_country_by_id($item['meta_value']);
						$itemArr['country_id'] = $country->country_name;
					}
					if($item['meta_key'] == 'shipper_id')
					{
						$shipper = $this->ion_auth->user($item['meta_value'])->row();
						if(!empty($shipper->company))
						{
							$itemArr['shipper_id'] = $shipper->company;
						}
					}
					$product_array = $itemArr;
				}
				$orders_list[] = array_merge($value, $product_array);
			}
			return $orders_list;
		}
	}
	
	private function order_serial($role_id, $order_id, $order_prefix, $timezone)
	{
		$orders = $this->users_model->get_role_joint($role_id, $taxonomy = 'user_order', $table = 'orders', $column = 'order_id', $is_object = TRUE);
		$orders_list = $this->get_orders($orders);
		$nm_gmt_time = local_to_gmt(strtotime('+ 1 month'), $timezone);
		$tm_gmt_time = local_to_gmt(strtotime('this month'), $timezone);
		$nextmonth = date('F', $nm_gmt_time);
		$thismonth = date('F', $tm_gmt_time);
		$lastday = date(strtotime("1 .$nextmonth. 2020"));
		$firstday = date(strtotime("1 .$thismonth. 2020"));
		$current_time = local_to_gmt(time(), $timezone);
		if(!empty($orders_list)) {
			foreach($orders_list as $order) {
				if($order['object_date_gmt'] > $firstday && $order['object_date_gmt'] < $lastday){
					$serials[] = $order['order_serial'];
				}
			}
			if(!empty($serials)){
				$max_serial_number = max($serials);
				$order_serial = $max_serial_number + 1;
				return $order_serial;
			} else {
				$order_serial = 1;
				return $order_serial;
			}
		} else {
			$order_serial = 1;
			return $order_serial;
		}
	}
}
