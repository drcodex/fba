<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Auth
 * @property Ion_auth|Ion_auth_model $ion_auth        The ION Auth spark
 * @property CI_Form_validation      $form_validation The form validation library
 */
class Products extends User_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->library(array('ion_auth', 'form_validation'));
		$this->load->helper(array('url', 'language'));
        $config =  array(
            'encrypt_name'    => TRUE,
			'upload_path'     => "./uploads/images/products/original/",
			'allowed_types'   => "gif|jpg|png|jpeg",
			'overwrite'       => FALSE,
			'max_size'        => "2000",
			'max_height'      => "2024",
			'max_width'       => "2024"
        );
        $this->load->library('upload', $config);
        $this->load->library('image_lib');
		$this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));
		$this->load->model('orders_model');
		$this->load->model('currencies_model');
		$this->load->model('locations_model');
		$this->load->model('products_model');
		$this->load->model('vendors_model');
		$this->load->model('shippers_model');
		$this->lang->load('auth');
		$this->access_type = 'user';
	}

	public function index()
	{
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect($this->access_type . '/login', 'refresh');
		}
		elseif (!$this->ion_auth->in_group($group = 3)) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
			$this->_render_error('errors' . DIRECTORY_SEPARATOR . '401', $this->data);
		}
		else
		{
			$products = $this->get_products($type = 'product', $parent = NULL, $author = NULL, $status = 'publish');
			if(!empty($products)){
				$this->data['products'] = $products;
			}
			//echo '<pre>'; print_r($products_list); echo '</pre>'; die();
			$this->data['title'] = lang('title_products');
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
			$this->_render_page($this->access_type . DIRECTORY_SEPARATOR . 'products' . DIRECTORY_SEPARATOR . 'index', $this->data);
		}
	}
	public function details($object_id)
	{
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect($this->access_type . '/auth/login', 'refresh');
		}
		elseif (!$this->ion_auth->in_group($group = 3)) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
			$this->_render_error('errors' . DIRECTORY_SEPARATOR . '401', $this->data);
		}
		else
		{
			$product_details = $this->get_product($object_id);
			//echo '<pre>';print_r($product_details);echo '</pre>';die();
			if(!empty($product_details)){
				$this->data['product'] = $product_details;
				$this->data['title'] = lang('title_products');
				$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
				$this->_render_page($this->access_type . DIRECTORY_SEPARATOR . 'products' . DIRECTORY_SEPARATOR . 'product_details', $this->data);
			}
			else
			{
				$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
				$this->_render_page('errors' . DIRECTORY_SEPARATOR . '404', $this->data);
			}
		}
	}
	public function get_products($type, $parent, $author, $status) {
		$products = $this->products_model->get_objects($type, $parent, $author, $status);
		if(!empty($products)){
			foreach ($products as $key => $value) {
				$assigned_vendors = $this->vendors_model->get_vendors_assigned_to_product($value['object_id']);
				$datestring = 'M, d Y';		
				$changeArr['object_id'] = $value['object_id'];
				$changeArr['object_type'] = $value['object_type'];
				$changeArr['object_parent'] = $value['object_parent'];
				$changeArr['object_status'] = $value['object_status'];
				$changeArr['object_title'] = $value['object_title'];
				$changeArr['object_author'] = $value['object_author'];
				$changeArr['object_content'] = $value['object_content'];
				$changeArr['object_excerpt'] = $value['object_excerpt'];
				$changeArr['object_slug'] = $value['object_slug'];
				$changeArr['comment_status'] = $value['comment_status'];
				$changeArr['comment_count'] = $value['comment_count'];
				$changeArr['object_date'] = date($datestring, $value['object_date']);
				$changeArr['object_date_gmt'] = date($datestring, $value['object_date_gmt']);
				$changeArr['object_modified'] = date($datestring, $value['object_modified']);
				$changeArr['object_modified_gmt'] = date($datestring, $value['object_modified_gmt']);
				$changeArr['menu_order'] = $value['menu_order'];
				$changeArr['product_image'] = $value['thumb'];
				$changeArr['assigned_vendors'] = $assigned_vendors;
				$products_array[] = $changeArr;
			}
			foreach ($products_array as $key => $value) {
				$productmeta = $this->products_model->get_objectmeta($value['object_id']);
				if(!empty($productmeta)){
					foreach ($productmeta as $key => $item) {
						$product_array = array();
						$itemArr[$item['meta_key']] = $item['meta_value'];
						$product_array = $itemArr;
					}
					$products_list[] = array_merge($value, $product_array);
				}
			}
			if(!empty($products_list)){
			    return $products_list;
			}
		}
	}
	public function get_product($object_id) {
		$product_type = $this->products_model->check_product_type($object_id);
		$product = $this->products_model->get_object($object_id);
		if(!empty($product)){
			$assigned_vendors = $this->vendors_model->get_vendors_assigned_to_product($object_id);
			$product_image = $this->products_model->get_product_image_by_product_id($object_id);
			$productmeta = $this->products_model->get_objectmeta($object_id);
			$product_variations = $this->get_products($type = 'product_variation', $parent = $object_id, $author = NULL, $status = 'publish');
			$datestring = 'M, d Y';		
			$changeArr['object_id'] = $product['object_id'];
			$changeArr['object_type'] = $product['object_type'];
			$changeArr['product_type'] = $product_type['product_type'];
			$changeArr['object_parent'] = $product['object_parent'];
			$changeArr['object_status'] = $product['object_status'];
			$changeArr['object_title'] = $product['object_title'];
			$changeArr['object_author'] = $product['object_author'];
			$changeArr['object_content'] = $product['object_content'];
			$changeArr['object_excerpt'] = $product['object_excerpt'];
			$changeArr['object_slug'] = $product['object_slug'];
			$changeArr['comment_status'] = $product['comment_status'];
			$changeArr['comment_count'] = $product['comment_count'];
			$changeArr['object_date'] = date($datestring, $product['object_date']);
			$changeArr['object_date_gmt'] = date($datestring, $product['object_date_gmt']);
			$changeArr['object_modified'] = date($datestring, $product['object_modified']);
			$changeArr['object_modified_gmt'] = date($datestring, $product['object_modified_gmt']);
			$changeArr['menu_order'] = $product['menu_order'];
			if(!empty($product_image->thumb)) {
				$changeArr['product_image'] = $product_image->thumb;
			}
			if(!empty($productmeta)){
				foreach ($productmeta as $key => $item) {
					$product_array = array();
					$changeArr[$item['meta_key']] = $item['meta_value'];
				}
			}
			$changeArr['assigned_vendors'] = $assigned_vendors;
			if($product_type = 'variable') {
				if(!empty($product_variations)){
					foreach ($product_variations as $k => $v) {
						$vArr['variable_title'] = $v['object_title'];
						$vArr['variable_regular_price'] = $v['variable_regular_price'];
						$vArr['variable_stock'] = $v['variable_stock'];
						$variation_array[] = $vArr;
					}
					$changeArr['variations'] = $variation_array;
				}
			}
			//echo '<pre>';print_r($changeArr);echo '</pre>';die();
			if(!empty($changeArr)){
			    return $changeArr;
			}
		}
	}
	public function ajax_fetch_products()
	{
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect($this->access_type . '/login', 'refresh');
		}
		elseif (!$this->ion_auth->in_group($group = 3)) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
			$this->_render_error('errors' . DIRECTORY_SEPARATOR . '401', $this->data);
		}
		else
		{
			$output = '';
			$query = '';
			if($this->input->post('product_search'))
			{
				$query = $this->input->post('product_search');
			}
			$taxonomy = 'variable';
			//echo '<pre>'; print_r($taxonomy); echo '</pre>'; die();
			$response = $this->products_model->fetch_data($query);
			$this->output->set_content_type('application/json');
			$this->output->set_output(json_encode($response));
		}
	}
	public function ajax_fetch_product_vendors()
	{
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect($this->access_type . '/login', 'refresh');
		}
		elseif (!$this->ion_auth->in_group($group = 3)) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
			$this->_render_error('errors' . DIRECTORY_SEPARATOR . '401', $this->data);
		}
		else
		{
			if($_POST)
			{
				$object_id = $this->input->post('object_id');
				if(!empty($object_id) && isset($object_id))
				{
					$this->form_validation->set_rules('object_id','Vendor','numeric');
					if ($this->form_validation->run() === TRUE)
					{
						$product = $this->vendors_model->check_product_type($object_id);
						if($product['product_variation'] = 'product_variation'){
							$assigned_vendors = $this->vendors_model->get_vendors_assigned_to_product($product['object_parent']);
							//echo '<pre>'; print_r($assigned_vendors); echo '</pre>'; die();
							$this->data['assigned_vendors'] = $assigned_vendors;

						}
					}
					$theHTMLResponse = $this->load->view($this->access_type . '/ajax_blocks/block_product_vendors', $this->data, true);
					$response = array('response'=>'yes','message'=>'Product variation added successfully','content'=>$theHTMLResponse);
					$this->output->set_content_type('application/json');
					$this->output->set_output(json_encode($response));
				}
				else
				{
					$response = array('response'=>'no','message'=>'Unauthorized Action');
					$this->output->set_content_type('application/json');
					$this->output->set_output(json_encode($response));
				}
			}
		}
	}
}
