<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Auth
 * @property Ion_auth|Ion_auth_model $ion_auth        The ION Auth spark
 * @property CI_Form_validation      $form_validation The form validation library
 */
class Orders extends Shipper_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->library(array('ion_auth', 'form_validation'));
		$this->load->helper(array('url', 'language'));

		$this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));
		$this->load->model('orders_model');
		$this->load->model('currencies_model');
		$this->load->model('locations_model');
		$this->load->model('users_model');
		$this->load->model('products_model');
		$this->load->model('couriers_model');
		$this->lang->load('auth');
		$this->access_type = 'shipper';
	}

	public function index()
	{
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect($this->access_type . '/login', 'refresh');
		}
		elseif (!$this->ion_auth->in_group($group = 5)) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
			$this->_render_error('errors' . DIRECTORY_SEPARATOR . '401', $this->data);
		}
		else
		{
			$shipper_id = $this->session->userdata('user_id');
			$orders = $this->orders_model->get_orders($type = 'order', $author = NULL);
			if(!empty($orders)){
				foreach ($orders as $key => $value) {
					$order = $this->get_order($value);
					if($shipper_id == isset($order['shipper_id'])){
						$orders_list[] = $order;
					}
				}
				//echo '<pre>'; print_r($orders_list); echo '</pre>'; die();
				$this->data['orders'] = $orders_list;
			}
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
			//echo '<pre>'; print_r($this->data); echo '</pre>'; die();
			$this->_render_page($this->access_type . DIRECTORY_SEPARATOR . 'orders' . DIRECTORY_SEPARATOR . 'index', $this->data);
		}
	}

	public function order_detail($object_id)
	{
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect($this->access_type . '/login', 'refresh');
		}
		elseif (!$this->ion_auth->in_group($group = 5)) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
			$this->_render_error('errors' . DIRECTORY_SEPARATOR . '401', $this->data);
		}
		else
		{
			$member_id = $this->session->userdata('user_id');
			if($_POST)
			{
				$this->form_validation->set_rules('date_created','Shipper','trim|required');
				$this->form_validation->set_rules('order_status','Tracking Number','trim|required');
				if ($this->form_validation->run() === TRUE)
				{
					$date_created = $this->input->post('date_created');
					$order_status = $this->input->post('order_status');
					$timezone  = 'UP5';
					$gmt_time = local_to_gmt($date_created, $timezone);
					$local_time = gmt_to_local($gmt_time, $timezone);
					$data = array(
						'object_status' => $order_status,
						'object_date' => $local_time,
						'object_date_gmt' => $gmt_time,
					);
					$this->orders_model->update_object($object_id, $data);
					$this->session->set_flashdata('class', 'btn btn-success waves-effect waves-light');
					$this->session->set_flashdata('message', 'Order detail is updated');
					redirect($this->access_type . "/orders/order_detail/".$object_id, 'refresh');
				}
			}
			$object = $this->orders_model->get_order('order', $object_id);
			$order = $this->get_order($object);
			
			$this->data['date_created'] = array(
				'name' => 'date_created',
				'id' => 'date_created',
				'type' => 'text',
				'value' => $this->form_validation->set_value('object_date', !empty($object['object_date']) ? $object['object_date'] : ""),
				'class' => 'form-control',
				'placeholder' => 'Month, DD YYYY',
			);
			$this->data['object_status'] = array(
				'name' => 'object_status',
				'id' => 'object_status',
				'type' => 'text',
				'value' => $this->form_validation->set_value('object_status', !empty($object['object_status']) ? $object['object_status'] : ""),
				'class' => 'form-control',
				'disabled' => 'disabled',
			);
			$this->data['order'] = $order;
			//echo '<pre>'; print_r($this->data['order']); echo '</pre>'; die();
			
			$this->data['class'] = $this->session->flashdata('class');
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
			$this->_render_page($this->access_type . DIRECTORY_SEPARATOR . 'orders' . DIRECTORY_SEPARATOR . 'order_detail', $this->data);
		}
	}
	public function add_tracking($order_id)
	{
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect($this->access_type . '/login', 'refresh');
		}
		elseif (!$this->ion_auth->in_group($group = 5)) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
			$this->_render_error('errors' . DIRECTORY_SEPARATOR . '401', $this->data);
		}
		else
		{
			$shipper_id = $this->session->userdata('user_id');
			if($_POST)
			{
				$tracking_number = $this->input->post('tracking_number');
				$courier_id = $this->input->post('courier_id');
			
				// validate form input
				$this->form_validation->set_rules('tracking_number','Tracking Number','trim|required');
				$this->form_validation->set_rules('courier_id','Courier','trim|required');
				if ($this->form_validation->run() === TRUE)
				{
					$metadata = array(
						'tracking_number' => $tracking_number,
						'courier_id' => $courier_id,
					);
					$this->compute_order_meta($order_id, $metadata);
					redirect($this->access_type . "/orders/view_orders", 'refresh');
				}
			}
			$meta = $this->orders_model->get_ordermeta_single($order_id);
			$this->data['tracking_number'] = array(
				'name' => 'tracking_number',
				'id' => 'tracking_number',
				'type' => 'text',
				'value' => $this->form_validation->set_value('tracking_number', !empty($meta['tracking_number']) ? $meta['tracking_number'] : ""),
				'class' => 'form-control',
				'placeholder' => 'Tracking Number',
			);
			$this->data['meta'] = $meta;
			$this->data['couriers'] = $this->couriers_model->get_couriers();
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
			//echo '<pre>'; print_r($this->data); echo '</pre>'; die();
			$this->_render_page($this->access_type . DIRECTORY_SEPARATOR . 'orders' . DIRECTORY_SEPARATOR . 'add_tracking', $this->data);
		}
	}

	public function get_order($value) {
		if(!empty($value)){
			$order_meta = $this->orders_model->get_ordermeta($value['order_id']);
			//echo '<pre>'; print_r($order_meta); echo '</pre>'; die();
			$datestring = 'M, d Y';		
			$orderArr['object_id'] = $value['object_id'];
			$orderArr['object_type'] = $value['object_type'];
			$orderArr['object_parent'] = $value['object_parent'];
			$orderArr['object_status'] = $value['object_status'];
			$orderArr['object_title'] = $value['object_title'];
			$orderArr['object_author'] = $value['object_author'];
			$orderArr['object_content'] = $value['object_content'];
			$orderArr['object_excerpt'] = $value['object_excerpt'];
			$orderArr['object_slug'] = $value['object_slug'];
			$orderArr['comment_status'] = $value['comment_status'];
			$orderArr['comment_count'] = $value['comment_count'];
			$orderArr['object_date'] = date($datestring, $value['object_date']);
			$orderArr['object_date_gmt'] = date($datestring, $value['object_date_gmt']);
			$orderArr['object_modified'] = date($datestring, $value['object_modified']);
			$orderArr['object_modified_gmt'] = date($datestring, $value['object_modified_gmt']);
			$orderArr['menu_order'] = $value['menu_order'];
			$orderArr['order_id'] = $value['order_id'];
			$orderArr['order_type'] = $value['order_type'];
			if(!empty($order_meta)){
				foreach ($order_meta as $key => $item) {
					$orderArr[$item['meta_key']] = $item['meta_value'];
					if($item['meta_key'] == 'country_id') {
						$country = $this->locations_model->get_country_by_id($item['meta_value']);
						$orderArr['country_id'] = $country->country_id;
						$orderArr['country_name'] = $country->country_name;
					}
					if($item['meta_key'] == 'vendor_id') {
						$vendor = $this->ion_auth->user($item['meta_value'])->row();
						if(!empty($vendor->company))
						{
							$orderArr['vendor_id'] = $vendor->id;
							$orderArr['vendor_name'] = $vendor->company;
						}
					}
					if($item['meta_key'] == 'shipper_id') {
						$shipper = $this->ion_auth->user($item['meta_value'])->row();
						if(!empty($shipper->company))
						{
							$orderArr['shipper_id'] = $shipper->id;
							$orderArr['shipper_name'] = $shipper->company;
						}
					}
					if($item['meta_key'] == 'courier_id')
					{
						$courier = $this->couriers_model->get_courier($item['meta_value']);
						if(!empty($courier))
						{
							$orderArr['courier_id'] = $item['meta_value'];
							$orderArr['courier_name'] = $courier->courier_name;
						}
					}
					if($item['meta_key'] == 'ship_by_date') {
						$orderArr['ship_by_date'] = date($datestring, $item['meta_value']);
					}
					if($item['meta_key'] == 'deliver_by_date') {
						$orderArr['deliver_by_date'] = date($datestring, $item['meta_value']);
					}
					$orderArray = $orderArr;
				}
			}
			if(!empty($orderArray)){
			    return $orderArray;
			}
		}
	}
	
	public function ajax_tracking_status()
	{
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect($this->access_type . '/login', 'refresh');
		}
		elseif (!$this->ion_auth->in_group($group = 5)) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
			$this->_render_error('errors' . DIRECTORY_SEPARATOR . '401', $this->data);
		}
		else
		{
			$user_id = $this->session->userdata('user_id');
			$credential = $this->settings_model->get_credential($user_id);
			if(!empty($credential['secret_key']))
			{
				if($_POST)
				{
					$object_id = $this->input->post('object_id');
					$order_id = $this->input->post('order_id');
					$tracking_number = $this->input->post('tracking_number');
					if(!empty($tracking_number) && isset($tracking_number))
					{
						$url = 'https://api-eu.dhl.com/track/shipments?trackingNumber='.$tracking_number;
						$data = array(
							"DHL-API-Key: ". $credential['secret_key'],
						);
						$curl = curl_init();
						curl_setopt_array($curl, array(
							CURLOPT_URL => $url,
							CURLOPT_RETURNTRANSFER => true,
							CURLOPT_ENCODING => "",
							CURLOPT_MAXREDIRS => 10,
							CURLOPT_TIMEOUT => 30,
							CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
							CURLOPT_CUSTOMREQUEST => "GET",
							CURLOPT_HTTPHEADER => $data,
						));
						$response = curl_exec($curl);
						$err = curl_error($curl);
						curl_close($curl);
						$result = json_decode($response);
						//echo '<pre>'; print_r($result); echo '</pre>'; die();
						if(!isset($result->status)){
							foreach($result->shipments as $shipment) {
								if(isset($shipment->status->status)){
									$status = $shipment->status->statusCode;
								} else {
									$description = $shipment->status->description;
									if($description == 'Shipment information received'){
										$status = "information received";
									} else {
										$description = $shipment->status->description;
									}
								}
							}
							$data = array(
								'object_status' => $status,
							);
							$this->orders_model->update_object($object_id, $data);
							$class = $this->class_type($status);
							$this->data['status'] = $status;
							$this->data['class'] = $class;
							//echo '<pre>'; print_r($status); echo '</pre>'; die();
							$theHTMLResponse = $this->load->view($this->access_type . '/ajax_blocks/tracking_status', $this->data, true);
							$response = array('response'=>'yes','message'=>'Tracking status updated to '.$status,'content'=>$theHTMLResponse);
							$this->output->set_content_type('application/json');
							$this->output->set_output(json_encode($response));
						} else {
							$data = array(
								'object_status' => 'invalid',
							);
							$class = $this->class_type($data['object_status']);
							$this->data['class'] = $class;
							if($result->status == '404'){
								$this->orders_model->update_object($object_id, $data);
								$this->data['status'] = $data['object_status'];
								$theHTMLResponse = $this->load->view($this->access_type . '/ajax_blocks/tracking_status', $this->data, true);
								$response = array('response'=>'no','title'=> $result->title,'class'=> $class,'message'=> $result->detail,'content'=>$theHTMLResponse);
								$this->output->set_content_type('application/json');
								$this->output->set_output(json_encode($response));
							} else {
								$this->data['status'] = 'unauthorized';
								$response = array('response'=>'no','title'=> $result->title, 'message'=> $result->detail);
								$this->output->set_content_type('application/json');
								$this->output->set_output(json_encode($response));
							}
						}
					} else {
						$response = array('response'=>'no','title'=>'No Tracking','message'=>'No tracking code assigned yet by shipper.');
						$this->output->set_content_type('application/json');
						$this->output->set_output(json_encode($response));
					}
				}
			} else {
				$response = array('response'=>'no','title'=>'API Secret Key Missing','message'=>'Get API secret key from DHL developer portal and Add it to API credentials in Settings');
				$this->output->set_content_type('application/json');
				$this->output->set_output(json_encode($response));
			}
		}
	}
	
	private function compute_order_meta($order_id, $metadata){
		$order_meta = $this->orders_model->get_ordermeta($order_id);
		foreach ($order_meta as $key => $object) {
			$o_meta[] = $object['meta_key'];
		}
		$meta_update = array();
		foreach ($metadata as $k => $v){
			$ordermeta = $this->orders_model->get_ordermeta_by_key($k, $order_id);
			$array['meta_key'] = "'".$k."'";
			if($this->input->post($k)) {
				$array['post_values'] = $this->input->post($k);
			} else {
				$array['post_values'] = $v;
			}
			if(!empty($o_meta)) {
				$key_exists = in_array($k, $o_meta);
				if($key_exists == 1){
					$meta_update[] = array(
						'order_meta_id'  => $ordermeta['order_meta_id'],
						'meta_key' => !empty($k) ? $k : "",
						'meta_value' => $array['post_values'],
					);
				} else {
					$meta_add[] = array(
						'order_id'  => $order_id,
						'meta_key' => !empty($k) ? $k : "",
						'meta_value' => $array['post_values'],
					);
				}
			} else {
				$meta_add[] = array(
					'order_id'  => $order_id,
					'meta_key' => !empty($k) ? $k : "",
					'meta_value' => $array['post_values'],
				);
			}
		}
		if(!empty($meta_update)){
			$this->orders_model->update_ordermeta($order_id, $meta_update);
		}
		if(!empty($meta_add)){
			//echo '<pre>';print_r($meta_add);echo '</pre>'; die();
			$this->orders_model->add_ordermeta($meta_add);
		}
	}
	private function class_type($status){
		switch ($status) {
			case "received":
				$class = 'info';
				break;
			case "transit":
				$class = 'warning';
				break;
			case "delivered":
				$class = 'success';
				break;
			case "invalid":
				$class = 'danger';
				break;
			case "unauthorized":
				$class = 'danger';
				break;
			default:
				$class = 'info';
		}
		return $class;
	}
}
