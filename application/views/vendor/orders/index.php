<div class="content dashboard">
	    <div class="container-fluid">
	        <!-- Page-Title -->
	        <div class="row">
	            <div class="col-sm-12">
	                <div class="page-header-2">
	                    <ol class="breadcrumb pull-right mb-0">
	                        <li class="breadcrumb-item active">Orders</li>
	                    </ol>
	                    <h5 class="page-title">Orders</h5>
	                    <div class="clearfix"></div>
	                </div>
	            </div>
	        </div>
	        <div class="row">
				<div class="col-lg-12">
					<div class="card-box">
						<div class="table-responsive">
							<table id="datatable" class="table table-hover mails m-0 table table-actions-bar">
								<thead>
								<tr>
									<th>Serial</th>
									<th>Name</th>
									<th>Status</th>
								</tr>
								</thead>
								<tbody>
									<?php if(!empty($orders)): foreach ($orders as $order): ?>
									<tr class="action-row">
										<td><?php echo $order['order_prefix'];?>-<?php echo $order['order_serial'];?>
										</td>
										<td>
											<?php echo $order['object_title'];?>
											<small id="emailHelp" class="form-text text-muted action-links">
												<a href="<?php echo base_url();?><?php echo $this->uri->segment(1);?>/orders/order_detail/<?php echo $order['object_id'];?>" class="text-success">Order Details</a>
											</small>
										</td>
										<td>
											<?php if($order['object_status'] == 'pending'):?>
												<span class="label label-primary"><?php echo $order['object_status'];?></span>
											<?php elseif($order['object_status'] == 'processing'):?>
												<span class="label label-info"><?php echo $order['object_status'];?></span>
											<?php elseif($order['object_status'] == 'onhold'):?>
												<span class="label label-inverse"><?php echo $order['object_status'];?></span>
											<?php elseif($order['object_status'] == 'completed'):?>
												<span class="label label-success"><?php echo $order['object_status'];?></span>
											<?php elseif($order['object_status'] == 'canceled'):?>
												<span class="label label-danger"><?php echo $order['object_status'];?></span>
											<?php elseif($order['object_status'] == 'refunded'):?>
												<span class="label label-warning"><?php echo $order['object_status'];?></span>
											<?php endif;?>
										</td>
									</tr>
									<?php endforeach; else:?>
									<tr>
										<td class="center text-center" colspan="9">No Records Found</td>
									</tr>
									<?php endif;?>
								</tbody>
							</table>
						</div>
					</div>
				</div> <!-- end col -->
			</div>
	    </div> <!-- container -->
	</div> <!-- content -->