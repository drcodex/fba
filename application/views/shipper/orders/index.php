<div class="content dashboard">
	    <div class="container-fluid">
	        <!-- Page-Title -->
	        <div class="row">
	            <div class="col-sm-12">
	                <div class="page-header-2">
	                    <ol class="breadcrumb pull-right mb-0">
	                        <li class="breadcrumb-item active">Orders</li>
	                    </ol>
	                    <h5 class="page-title">Orders</h5>
	                    <div class="clearfix"></div>
	                </div>
	            </div>
	        </div>
	        <div class="row">
				<div class="col-lg-12">
					<div class="card-box">
						<div class="table-responsive">
							<table id="datatable" class="table table-hover mails m-0 table table-actions-bar">
								<thead>
								<tr>
									<th>Serial</th>
									<th>Name</th>
									<th>Courier</th>
									<th>Tracking No</th>
									<th width="120px">Status</th>
									<th>Customer</th>
									<th>Country</th>
									<th width="100px">Action</th>
								</tr>
								</thead>
								<tbody>
									<?php if(!empty($orders)): foreach ($orders as $order): ?>
									<tr class="action-row">
										<td>
											<?php echo $order['order_prefix'];?>-<?php echo $order['order_serial'];?>
										</td>
										<td>
											<?php echo $order['object_title'];?>
											<small id="emailHelp" class="form-text text-muted action-links">
												<a href="<?php echo base_url();?><?php echo $this->uri->segment(1);?>/orders/order_detail/<?php echo $order['object_id'];?>" class="text-success">Order Details</a>
											</small>
										</td>
										<td>
											<?php if(isset($order['courier_name'])):?>
											<b><?php echo $order['courier_name'];?></b>
											<?php endif;?>
										</td>
										<td><?php echo $order['tracking_number'];?></td>
										<td>
											<div class="status-<?php echo $order['object_id'];?>">
												<?php if($order['object_status'] == 'pending'):?>
													<span class="label label-primary"><?php echo $order['object_status'];?></span>
												<?php elseif($order['object_status'] == 'information received'):?>
													<span class="label label-info"><?php echo $order['object_status'];?></span>
												<?php elseif($order['object_status'] == 'transit'):?>
													<span class="label label-warning"><?php echo $order['object_status'];?></span>
												<?php elseif($order['object_status'] == 'onhold'):?>
													<span class="label label-inverse"><?php echo $order['object_status'];?></span>
												<?php elseif($order['object_status'] == 'delivered'):?>
													<span class="label label-success"><?php echo $order['object_status'];?></span>
												<?php elseif($order['object_status'] == 'invalid'):?>
													<span class="label label-danger"><?php echo $order['object_status'];?></span>
												<?php elseif($order['object_status'] == 'refunded'):?>
													<span class="label label-danger"><?php echo $order['object_status'];?></span>
												<?php endif;?>
											</div>
										</td>
										<td><?php echo $order['customer_name'];?></td>
										<td><?php echo $order['country_name'];?></td>
										<td><a href="<?php echo base_url();?><?php echo $this->uri->segment(1);?>/orders/add_tracking/<?php echo $order['order_id'];?>" class="btn btn-icon waves-effect waves-light btn-gold"><i class="ti-truck"></i></a>
										<a id="update_tracking_status" data-style="zoom-in" data-object-id="<?php echo $order['object_id'];?>" data-order-id="<?php echo $order['order_id'];?>" data-tracking-number="<?php echo $order['tracking_number'];?>" class="btn btn-gold waves-effect waves-light">Track</a></td>
									</tr>
									<?php endforeach; else:?>
									<tr>
										<td class="center text-center" colspan="9">No Records Found</td>
									</tr>
									<?php endif;?>
								</tbody>
							</table>
						</div>
					</div>
				</div> <!-- end col -->
			</div>
	    </div> <!-- container -->
	</div> <!-- content -->
	<script>
	$(document).on("click", "#update_tracking_status", function(e) {
		e.preventDefault(); // avoid to execute the actual submit of the form.
		e.stopPropagation();
		var l = Ladda.create(this);
		l.start();
		var object_id = $(this).data('object-id');
		var order_id = $(this).data('order-id');
		var tracking_number = $(this).data('tracking-number');
		var form_data = new FormData();
		form_data.append("object_id", object_id)
		form_data.append("tracking_number", tracking_number)
		var url = base_url + account_type + "/orders/ajax_tracking_status";

		$.ajax({
			url: url,
			type: "POST",
			async:"false",
			dataType: "html",
			cache:false,
			contentType: false,
			processData: false,
			data: form_data, // serializes the form's elements.
			success: function(data)
			{				
				data = JSON.parse(data);
				if(data.response == "yes")
				{
					l.stop();
					$( ".status-" + object_id ).replaceWith(data.content);
					$.Notification.autoHideNotify('custom', 'top right', 'Success', data.message);
				} else {
					l.stop();
					if(data.class){
						$( ".status-" + object_id ).replaceWith(data.content);
						$.Notification.autoHideNotify('error', 'top right', data.title, data.message);
					} else {
						$.Notification.autoHideNotify('error', 'top right', 'Unauthorized', data.message);
					}
				}
			}
		});
	});
	</script>