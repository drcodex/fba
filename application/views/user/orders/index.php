<div class="content dashboard">
	    <div class="container-fluid">
	        <!-- Page-Title -->
	        <div class="row">
	            <div class="col-sm-12">
	                <div class="page-header-2">
	                    <ol class="breadcrumb pull-right mb-0">
	                        <li class="breadcrumb-item active">Orders</li>
	                    </ol>
	                    <h5 class="page-title">Orders</h5>
						<a href="<?php echo base_url();?><?php echo $this->uri->segment(1);?>/orders/place_order"
                        class="btn btn-gold waves-effect waves-light">Place New Order</a>
	                    <div class="clearfix"></div>
	                </div>
	            </div>
	        </div>
	        <div class="row">
				<div class="col-lg-12">
					<div class="card-box">
						<div class="table-responsive">
							<table id="datatable" class="table table-hover mails m-0 table table-actions-bar">
								<thead>
								<tr>
									<th>Serial</th>
									<th>Date</th>
									<th>Name</th>
									<th>Courier</th>
									<th>Tracking No</th>
									<th>Status</th>
									<th>Customer</th>
									<th>Country</th>
								</tr>
								</thead>
								<tbody>
									<?php if(!empty($orders)): foreach ($orders as $order): ?>
									<tr class="action-row">
										<td>
											<?php echo $order['order_prefix'];?>-<?php echo $order['order_serial'];?>
										</td>
										<td>
											<?php echo date('M, d Y', $order['object_date']);?>
										</td>
										<td>
											<?php echo $order['object_title'];?>
											<small id="emailHelp" class="form-text text-muted action-links">
												<a href="<?php echo base_url();?><?php echo $this->uri->segment(1);?>/orders/order_detail/<?php echo $order['object_id'];?>" class="text-success">Details</a> | <a href="<?php echo base_url();?><?php echo $this->uri->segment(1);?>/orders/edit_order/<?php echo $order['object_id'];?>" class="text-primary">Edit</a> | <a href="<?php echo base_url();?><?php echo $this->uri->segment(1);?>/orders/delete_order/<?php echo $order['object_id'];?>" class="text-danger">Delete</a>
											</small>
										</td>
										<td>
											<b><?php echo $order['shipper_id'];?></b>
										</td>
										<td><?php echo $order['tracking_number'];?></td>
										<td>
											<div class="status-<?php echo $order['object_id'];?>">
												<?php if($order['object_status'] == 'pending'):?>
													<span class="label label-primary"><?php echo $order['object_status'];?></span>
												<?php elseif($order['object_status'] == 'information received'):?>
													<span class="label label-info"><?php echo $order['object_status'];?></span>
												<?php elseif($order['object_status'] == 'transit'):?>
													<span class="label label-warning"><?php echo $order['object_status'];?></span>
												<?php elseif($order['object_status'] == 'onhold'):?>
													<span class="label label-inverse"><?php echo $order['object_status'];?></span>
												<?php elseif($order['object_status'] == 'delivered'):?>
													<span class="label label-success"><?php echo $order['object_status'];?></span>
												<?php elseif($order['object_status'] == 'invalid'):?>
													<span class="label label-danger"><?php echo $order['object_status'];?></span>
												<?php elseif($order['object_status'] == 'refunded'):?>
													<span class="label label-danger"><?php echo $order['object_status'];?></span>
												<?php endif;?>
											</div>
										</td>
										<td><?php echo $order['customer_name'];?></td>
										<td><?php echo $order['country_id'];?></td>
									</tr>
									<?php endforeach; else:?>
									<tr>
										<td class="center text-center" colspan="9">No Records Found</td>
									</tr>
									<?php endif;?>
								</tbody>
							</table>
						</div>
					</div>
				</div> <!-- end col -->
			</div>
	    </div> <!-- container -->
	</div> <!-- content -->